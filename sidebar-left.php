<?php
/**
 * Displays the left sidebar of the theme.
 *
 */
?>

<?php
/**
 * fituet_before_left_sidebar
 */
do_action( 'fituet_before_left_sidebar' );
?>

<?php
/**
 * fituet_left_sidebar hook
 *
 * HOOKED_FUNCTION_NAME PRIORITY
 *
 * fituet_display_left_sidebar 10
 */
do_action( 'fituet_left_sidebar' );
?>

<?php
/**
 * fituet_after_left_sidebar
 */
do_action( 'fituet_after_left_sidebar' );
?>