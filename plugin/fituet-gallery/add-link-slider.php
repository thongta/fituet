<?php
/**
 * Created by PhpStorm.
 * User: Tu TV
 * Date: 27/8/2015
 * Time: 10:09 PM
 */

add_action( 'add_meta_boxes', 'fituet_add_link_slider_meta_box' );
function fituet_add_link_slider_meta_box() {

	$screens = array( 'fituet_slide' );

	foreach ( $screens as $screen ) {

		add_meta_box(
			'fituet_link_slide',
			__( 'Link to: ', 'fituet' ),
			'fituet_link_slider_meta_box_callback',
			$screen
		);
	}
}

function fituet_link_slider_meta_box_callback( $post ) {

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'fituet_save_link_slider_meta_box_data', 'fituet_link_slider_meta_box_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	$value = get_post_meta( $post->ID, 'fituet_link_slider_meta_box_nonce', true );

	echo '<input class="widefat" type="text" id="fituet_link_slider_field" name="fituet_link_slider_field" value="' . esc_attr( $value ) . '" />';
}


/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function fituet_save_link_slider_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['fituet_link_slider_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['fituet_link_slider_meta_box_nonce'], 'fituet_save_link_slider_meta_box_data' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['fituet_slide'] ) && 'page' == $_POST['fituet_slide'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	/* OK, it's safe for us to save the data now. */

	// Make sure that it is set.
	if ( ! isset( $_POST['fituet_link_slider_field'] ) ) {
		return;
	}

	// Sanitize user input.
	$my_data = sanitize_text_field( $_POST['fituet_link_slider_field'] );

	// Update the meta field in the database.
	update_post_meta( $post_id, 'fituet_link_slider_meta_box_nonce', $my_data );
}

add_action( 'save_post', 'fituet_save_link_slider_meta_box_data' );