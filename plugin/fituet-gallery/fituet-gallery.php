<?php
/**
 * Plugin Name: FIT Gallery
 * Plugin URI: http://thimpress.com
 * Description: Custom slider
 * Version: 1.1
 * Author: Tu TV
 * Author URI: http://tutran.me
 * License: GNU General Public License v2 or later
 */

function fituet_create_post_type_slide() {

	$labels = array(
		'name'               => _x( 'Slides', 'Post Type General Name', 'fituet' ),
		'singular_name'      => _x( 'Slide', 'Post Type Singular Name', 'fituet' ),
		'menu_name'          => __( 'Slide', 'fituet' ),
		'name_admin_bar'     => __( 'Slide', 'fituet' ),
		'parent_item_colon'  => __( 'Parent Slide:', 'fituet' ),
		'all_items'          => __( 'All Slides', 'fituet' ),
		'add_new_item'       => __( 'Add New Slide', 'fituet' ),
		'add_new'            => __( 'Add New', 'fituet' ),
		'new_item'           => __( 'New Slide', 'fituet' ),
		'edit_item'          => __( 'Edit Slide', 'fituet' ),
		'update_item'        => __( 'Update Slide', 'fituet' ),
		'view_item'          => __( 'View Slide', 'fituet' ),
		'search_items'       => __( 'Search Slide', 'fituet' ),
		'not_found'          => __( 'Not found', 'fituet' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'fituet' ),
	);
	$args   = array(
		'label'               => __( 'Slide', 'fituet' ),
		'description'         => __( 'Custom Slide', 'fituet' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'menu_icon'           => 'dashicons-format-gallery',
	);
	register_post_type( 'fituet_slide', $args );

}

add_action( 'init', 'fituet_create_post_type_slide', 0 );

require_once 'add-link-slider.php';
require_once 'fituet-link-slider-filter.php';
