<?php
/**
 * Created by PhpStorm.
 * User: Tu TV
 * Date: 16/10/2015
 * Time: 12:33 AM
 */

/**
 * Show event upcoming
 *
 * @param $show
 */
function fituet_events_upcoming_loop_to_widget( $show ) {
	$args = array(
		'post_type'      => array( 'fituet_event' ),
		'posts_per_page' => - 1,
	);

	$the_query = new WP_Query( $args );

	/**
	 * Filter the upcoming event
	 */
	$date_format = 'd/m/Y';
	$arr         = array();

	$today = date_create( 'now' );
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();

			$date_event = get_post_meta( get_the_ID(), 'fit_event_date', true );
			$date_event = date_create()->createFromFormat(
				$date_format,
				$date_event
			);
			if ( $date_event->getTimestamp() >= $today->getTimestamp() ) {
				$arr[get_the_ID()] = $date_event->getTimestamp();
			}
		}
	}
	/* Restore original Post Data */
	wp_reset_postdata();

	/**
	 * Show the upcoming event post
	 */
	asort( $arr );
	$post_event_ids = array_keys( $arr );
	if ( count( $post_event_ids ) > 0 ) {
		echo '<div class="fituet-event vticker">';
		echo '<ul class="list-events" data-number="' . $show . '">';
		foreach ( $post_event_ids as $id ) {
			$date_event = get_post_meta( $id, 'fit_event_date', true );
			$date_event = date_create()->createFromFormat(
				$date_format,
				$date_event
			);

			echo '<li number-date="' . get_post_meta( $id, 'fit_event_date', true ) . '">
					<div class="left">
						<span class="day">' . $date_event->format( 'd' ) . '</span>
						<span class="moth">' . $date_event->format( 'M' ) . '</span>
					</div>
					<a href="'
				. esc_url( get_permalink( $id ) ) . '" title="'
				. esc_attr( get_the_title( $id ) ) . '">'
				. get_the_title( $id ) . '</a>
				</li>';
		}
		echo '</ul>';
		echo '</div>';
	}

	/**
	 * Register script
	 */
	wp_enqueue_script(
		'jquery-vticker',
		FIT_EVENT_URL . 'assets/js/jquery.vticker.min.js', array( 'jquery' )
	);
	wp_enqueue_script(
		'widget-event',
		FIT_EVENT_URL . 'assets/js/widget-event.js',
		array( 'jquery', 'jquery-vticker' )
	);
}

function fituet_event_get_time( $post_id ) {
	$args = array(
		'type' => 'date',
	);
	$date = rwmb_meta( 'fit_event_date', $args, $post_id );

	phpinfo();

	return $date;
}