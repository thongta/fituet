<?php
/**
 * Created by PhpStorm.
 * User: Tu TV
 * Date: 13/10/2015
 * Time: 4:20 PM
 */

require_once 'metaboxio/meta-box.php';

add_filter( 'rwmb_meta_boxes', 'fituet_register_meta_boxes_event' );
function fituet_register_meta_boxes_event( $meta_boxes ) {
	$prefix = 'fit_event_';

	$meta_boxes[] = array(
		'id'         => $prefix . 'times',
		'title'      => __( 'Time event', 'fituet' ),
		'post_types' => array( 'fituet_event' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'fields'     => array(
			array(
				'id'         => $prefix . 'date',
				'name'       => 'Date',
				'desc'       => '',
				'type'       => 'date',
				'std'        => date( 'd/m/Y' ),
				'js_options' => array(
					'appendText'      => '(dd/mm/yyyy)',
					'autoSize'        => true,
					'buttonText'      => __( 'Select Date', 'fituet' ),
					'dateFormat'      => 'dd/mm/yy',
					'numberOfMonths'  => 1,
					'showButtonPanel' => true,
				),
			),
		),
	);

	return $meta_boxes;
}