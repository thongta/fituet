<?php
/**
 * Plugin Name: FIT Events
 * Plugin URI: http://thimpress.com
 * Description: Events Manager
 * Version: 1.0
 * Author: Tu TV
 * Author URI: http://tutran.me
 * License: GNU General Public License v2 or later
 */

define( 'FIT_EVENT_DIR', plugin_dir_path( __FILE__ ) );
define( 'FIT_EVENT_URL', plugin_dir_url( __FILE__ ) );

function fituet_register_event_post_type() {

	$labels  = array(
		'name'               => _x(
			'Events', 'Post Type General Name',
			'fituet'
		),
		'singular_name'      => _x(
			'Event', 'Post Type Singular Name',
			'fituet'
		),
		'menu_name'          => __( 'Event', 'fituet' ),
		'name_admin_bar'     => __( 'Event', 'fituet' ),
		'parent_item_colon'  => __( 'Parent Event:', 'fituet' ),
		'all_items'          => __( 'All Events', 'fituet' ),
		'add_new_item'       => __( 'Add New Event', 'fituet' ),
		'add_new'            => __( 'Add New', 'fituet' ),
		'new_item'           => __( 'New Event', 'fituet' ),
		'edit_item'          => __( 'Edit Event', 'fituet' ),
		'update_item'        => __( 'Update Event', 'fituet' ),
		'view_item'          => __( 'View Event', 'fituet' ),
		'search_items'       => __( 'Search Event', 'fituet' ),
		'not_found'          => __( 'Not found', 'fituet' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'fituet' ),
	);
	$rewrite = array(
		'slug'       => 'event',
		'with_front' => true,
		'pages'      => true,
		'feeds'      => true,
	);
	$args    = array(
		'label'               => __( 'Event', 'fituet' ),
		'description'         => __( 'FIT Events', 'fituet' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 7,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
		'menu_icon'           => 'dashicons-calendar',
	);
	register_post_type( 'fituet_event', $args );

}

add_action( 'init', 'fituet_register_event_post_type', 0 );

/**
 * Add meta box
 */
require_once 'add-metabox.php';

/**
 * Functions
 */
require_once 'functions.php';

/**
 * Register widget
 */
require_once 'event-widget.php';