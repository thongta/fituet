/**
 * Created by tutv95 on 10/10/2015.
 */

jQuery(document).ready(function ($) {
    function hide_all_publication_type() {
        $('#article').hide();
        $('#inproceedings').hide();
        $('#book').hide();
        $('#misc').hide();
    }

    var select_type_publication = $('#type_publication .rwmb-select');

    hide_all_publication_type();
    $('#' + select_type_publication.val()).show();

    select_type_publication.on('change', function () {
        hide_all_publication_type();
        $('#' + $(this).val()).show();
    });
});