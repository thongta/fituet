<?php
/**
 * Created by PhpStorm.
 * User: Tu TV
 * Date: 10/10/2015
 * Time: 1:10 AM
 */

/**
 * Meta box
 * https://metabox.io
 */
require_once 'metaboxio/meta-box.php';

function fituet_translate_publication_type( $type_val ) {
	$types = array(
		'article'       => __( 'Article', 'fituet' ),
		'inproceedings' => __( 'Inproceedings', 'fituet' ),
		'book'          => __( 'Book', 'fituet' ),
		'misc'          => __( 'Misc', 'fituet' ),
	);

	if ( ! isset( $types[$type_val] ) ) {
		// Default is article
		return $types['article'];
	}

	return $types[$type_val];
}

/**
 * Select type for publication
 */
function fituet_get_select_type_publication() {
	$types = array(
		'article'       => fituet_translate_publication_type( 'article' ),
		'inproceedings' => fituet_translate_publication_type( 'inproceedings' ),
		'book'          => fituet_translate_publication_type( 'book' ),
		'misc'          => fituet_translate_publication_type( 'misc' ),
	);

	return $types;
}

function fituet_get_user_teachers() {
	$args = array(
		'blog_id'      => $GLOBALS['blog_id'],
		'role'         => '',
		'meta_key'     => '',
		'meta_value'   => '',
		'meta_compare' => '',
		'meta_query'   => array(),
		'date_query'   => array(),
		'include'      => array(),
		'exclude'      => array(),
		'orderby'      => 'login',
		'order'        => 'ASC',
		'offset'       => '',
		'search'       => '',
		'number'       => '',
		'count_total'  => false,
		'fields'       => 'all',
		'who'          => ''
	);

	$users = get_users( $args );

	$id_user_teachers = array();

	foreach ( $users as $user ) {
		if ( bp_get_member_type( $user->data->ID ) == 'fituet_teacher' ) {
			$id_user_teachers[$user->data->ID] = $user->data->display_name;
		}
	}

	return $id_user_teachers;
}

add_filter( 'rwmb_meta_boxes', 'fituet_register_meta_boxes_publication' );
function fituet_register_meta_boxes_publication( $meta_boxes ) {
	$prefix = 'fituet_';

	$types = fituet_get_select_type_publication();
	fituet_get_user_teachers();

	/**
	 * Publication Type
	 */
	$meta_boxes[] = array(
		'id'         => 'type_publication',
		'title'      => __( 'Publication type', 'fituet' ),
		'post_types' => array( 'publication' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'fields'     => array(
			array(
				'id'      => 'type_pub',
				'name'    => 'Type',
				'desc'    => '',
				'type'    => 'select',
				'options' => $types,
				'std'     => 'article',
			),
		),
	);

	// @ARTICLE
	$meta_boxes[] = array(
		'id'         => 'article',
		'title'      => __( 'Article', 'fituet' ),
		'post_types' => array( 'publication' ),
		'context'    => 'normal',
		'priority'   => 'low',
		'fields'     => array(
			array(
				'id'   => 'article_journal',
				'name' => 'Journal',
				'desc' => '',
				'type' => 'text',
			),
			array(
				'id'   => 'article_volume',
				'name' => 'Volume',
				'desc' => '',
				'type' => 'text',
			),
			array(
				'id'   => 'article_issue',
				'name' => 'Issue',
				'desc' => '',
				'type' => 'text',
			),
			array(
				'id'   => 'article_pages',
				'name' => 'Pages',
				'desc' => '',
				'type' => 'text',
			),
		),
	);

	// @INPROCEEDINGS
	$meta_boxes[] = array(
		'id'         => 'inproceedings',
		'title'      => __( 'Inproceedings', 'fituet' ),
		'post_types' => array( 'publication' ),
		'context'    => 'normal',
		'priority'   => 'low',
		'fields'     => array(
			array(
				'id'   => 'inproceedings_booktitle',
				'name' => 'Book title',
				'desc' => '',
				'type' => 'text',
			),
			array(
				'id'   => 'inproceedings_publisher',
				'name' => 'Publisher',
				'desc' => '',
				'type' => 'text',
			),
			array(
				'id'   => 'inproceedings_address',
				'name' => 'Address',
				'desc' => '',
				'type' => 'text',
			),
			array(
				'id'   => 'inproceedings_pages',
				'name' => 'Pages',
				'desc' => '',
				'type' => 'text',
			),
		),
	);

	// @BOOK
	$meta_boxes[] = array(
		'id'         => 'book',
		'title'      => __( 'Book', 'fituet' ),
		'post_types' => array( 'publication' ),
		'context'    => 'normal',
		'priority'   => 'low',
		'fields'     => array(
			array(
				'id'   => 'book_publisher',
				'name' => 'Publisher',
				'desc' => '',
				'type' => 'text',
			),
			array(
				'id'   => 'book_address',
				'name' => 'Address',
				'desc' => '',
				'type' => 'text',
			),
		),
	);

	// @MISC
	$meta_boxes[] = array(
		'id'         => 'misc',
		'title'      => __( 'Misc', 'fituet' ),
		'post_types' => array( 'publication' ),
		'context'    => 'normal',
		'priority'   => 'low',
		'fields'     => array(
			array(
				'id'   => 'misc_howpublished',
				'name' => 'How published',
				'desc' => '',
				'type' => 'text',
			),
			array(
				'id'   => 'misc_month',
				'name' => 'Month',
				'desc' => '',
				'type' => 'number',
			),
			array(
				'id'   => 'misc_key',
				'name' => 'Key',
				'desc' => '',
				'type' => 'text',
			),
			array(
				'id'   => 'misc_note',
				'name' => 'Note',
				'desc' => '',
				'type' => 'textarea',
			),
		),
	);

	/**
	 * Year and Authors
	 */
	$meta_boxes[] = array(
		'id'         => 'year_authors',
		'title'      => __( 'Year & Authors', 'fituet' ),
		'post_types' => array( 'publication' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'fields'     => array(
			array(
				'id'       => 'author_pub',
				'name'     => 'Author',
				'desc'     => '',
				'type'     => 'select_advanced',
				'options'  => fituet_get_user_teachers(),
				'multiple' => true,
			),
			array(
				'id'   => 'year_pub',
				'name' => 'Year',
				'desc' => '',
				'type' => 'number',
				'std'  => date( 'Y' ),
			),
		),
	);

	return $meta_boxes;
}

function fituet_enqueue_script_publication_admin( $hook ) {
	global $post;

	if ( $post->post_type === 'publication' ) {
		wp_enqueue_script( 'script-publication', FIT_PUB_ASSET_URL . 'js/select-type.js', array( 'jquery' ) );
	}
}

add_action( 'admin_enqueue_scripts', 'fituet_enqueue_script_publication_admin', 10, 1 );