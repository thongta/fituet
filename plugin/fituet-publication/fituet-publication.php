<?php
/**
 * Plugin Name: FIT Publication
 * Plugin URI: http://thimpress.com
 * Description: FIT Publication
 * Version: 1.0
 * Author: Tu TV
 * Author URI: http://tutran.me
 * License: GNU General Public License v2 or later
 */

require_once 'plugin-required.php';

define( 'FIT_PUB_DIR', plugin_dir_path( __FILE__ ) );
define( 'FIT_PUB_URL', plugin_dir_url( __FILE__ ) );
define( 'FIT_PUB_ASSET_URL', FIT_PUB_URL . 'assets/' );

/**
 * Register Publication Post Type
 */
function fituet_create_post_type_publication() {

	$labels = array(
		'name'               => _x(
			'Publications', 'Post Type General Name',
			'fituet'
		),
		'singular_name'      => _x(
			'Publication', 'Post Type Singular Name',
			'fituet'
		),
		'menu_name'          => __( 'Publication', 'fituet' ),
		'name_admin_bar'     => __( 'Publication', 'fituet' ),
		'parent_item_colon'  => __( 'Parent Item:', 'fituet' ),
		'all_items'          => __( 'All Publications', 'fituet' ),
		'add_new_item'       => __( 'Add New Publication', 'fituet' ),
		'add_new'            => __( 'Add New', 'fituet' ),
		'new_item'           => __( 'New Publication', 'fituet' ),
		'edit_item'          => __( 'Edit Publication', 'fituet' ),
		'update_item'        => __( 'Update Publication', 'fituet' ),
		'view_item'          => __( 'View Publication', 'fituet' ),
		'search_items'       => __( 'Search Publication', 'fituet' ),
		'not_found'          => __( 'Not found', 'fituet' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'fituet' ),
	);
	$args   = array(
		'label'               => __( 'Publication', 'fituet' ),
		'description'         => __( 'Publication', 'fituet' ),
		'labels'              => $labels,
		'supports'            => array( 'title' ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'menu_icon'           => 'dashicons-id-alt',
	);
	register_post_type( 'publication', $args );

}

add_action( 'init', 'fituet_create_post_type_publication', 0 );

/**
 * Initialize when BuddyPress was installed!
 */
function fituet_publication_init() {
	wp_register_style(
		'custom-publication',
		FIT_PUB_URL . 'template/assets/css/custom-publication.css'
	);
	wp_register_script(
		'custom-publication-js',
		FIT_PUB_URL . 'template/assets/js/custom-publication.js',
		array( 'jquery' )
	);

	require_once 'functions.php';
	require_once 'add-metabox.php';

	/**
	 * Custom template publication
	 */
	add_filter( 'single_template', 'fituet_custom_template_publication' );

	function fituet_custom_template_publication( $single ) {
		global $wp_query, $post;

		if ( $post->post_type == 'publication' ) {
			if ( file_exists(
				dirname( __FILE__ )
				. '/template/single-publication.php'
			) ) {
				fituet_enqueue_script_publication_frontend();

				return dirname( __FILE__ ) . '/template/single-publication.php';
			}
		}

		return $single;
	}

	/**
	 * Enqueue script & style for @publication type
	 */
	function fituet_enqueue_script_publication_frontend() {
		wp_enqueue_style( 'custom-publication' );
	}
}

add_action( 'bp_include', 'fituet_publication_init' );