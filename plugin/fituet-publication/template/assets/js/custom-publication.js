/**
 * Created by tutv95 on 11/10/2015.
 */

jQuery(document).ready(function ($) {
    var pubs = $('#scientific_articles > .publications');

    var years = [];

    pubs.each(function (index) {
        var year = $(this).attr('data-year');
        if (years.indexOf(year) === -1) {
            years.push(year);
        }
    });

    // Sort year
    years.sort(function (a, b) {
        return a - b
    });

    for (var i = 0; i < years.length; i++) {
        var sort_date = '<div class="sort_date">'
            + '<h3>' + years[i] + '</h3>';

        pubs.each(function (index) {
            var year = $(this).attr('data-year');

            if (year === years[i]) {
                var pub = $(this).detach();

                sort_date += pub.html();
            }
        });

        sort_date += '</div>';

        $('#scientific_articles').append(sort_date);
    }

    console.log(years);
});

