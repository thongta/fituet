<?php
/**
 * Created by PhpStorm.
 * User: Tu TV
 * Date: 10/10/2015
 * Time: 10:59 PM
 */

/**
 * Get @publication type value
 *
 * @param $post_id
 *
 * @return mixed
 */
function fituet_get_publication_type_val( $post_id ) {
	$args = array(
		'type' => 'text',
	);

	return rwmb_meta( 'type_pub', $args, $post_id );
}

/**
 * Get @publication type
 *
 * @param $post_id
 *
 * @return mixed
 */
function fituet_get_publication_type( $post_id ) {
	$type_val = fituet_get_publication_type_val( $post_id );

	return fituet_translate_publication_type( $type_val );
}

/**
 * Get year of @publication
 *
 * @param $post_id
 *
 * @return mixed
 */
function fituet_get_publication_year( $post_id ) {
	$args = array(
		'type' => 'number',
	);

	return rwmb_meta( 'year_pub', $args, $post_id );
}


function fituet_get_publication_authors( $post_id ) {
	$args = array(
		'multiple' => true,
	);

	$author_ids = rwmb_meta( 'author_pub', $args, $post_id );

	if ( empty( $author_ids ) || count( $author_ids ) == 0 ) {
		return null;
	}

	return $author_ids;
}

/**
 * Get list authors of publication
 *
 * @param $post_id
 *
 * @return null|string
 */
function fituet_get_publication_authors_html( $post_id ) {
	$author_ids = fituet_get_publication_authors( $post_id );

	$html = '';

	foreach ( $author_ids as $index => $author ) {
		$html .= '<a href="' . bp_core_get_user_domain( $author ) . '">' . bp_core_get_user_displayname( $author ) . '</a>';

		if ( $index < count( $author_ids ) - 1 ) {
			$html .= ', ';
		}
	}

	return $html;
}

/**
 * Get custom field of publication
 *
 * @param $post_id
 * @param $field
 *
 * @return mixed
 */
function fituet_get_publication_custom_field( $post_id, $field ) {
	$args = array(
		'type' => 'text',
	);

	return rwmb_meta( $field, $args, $post_id );
}

function fituet_get_publication_ids_by_user_id( $user_id ) {
	$publication_ids = array();

	$args = array(
		'post_type'      => 'publication',
		'orderby'        => 'post_date',
		'order'          => 'ASC',
		'posts_per_page' => - 1,
	);

	$the_query = new WP_Query( $args );

	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();

			$post_id = $the_query->post->ID;

			$author_ids = fituet_get_publication_authors( $post_id );

			if ( count( $author_ids ) > 0 ) {
				foreach ( $author_ids as $author ) {
					if ( $author == $user_id ) {
						array_push( $publication_ids, $post_id );
						break;
					}
				}
			}
		}
	}
	wp_reset_postdata();

	return $publication_ids;
}

function fituet_scientific_articles_header( $user_id ) {
	if ( count( fituet_get_publication_ids_by_user_id( $user_id ) ) > 0 ) {
		?>
		<span class="scientific_articles"
			  data-tab="scientific_articles"><?php _e( 'Scientific articles', 'fituet' ); ?></span>
		<?php
	}
}

add_action( 'fituet_scientific_articles_header', 'fituet_scientific_articles_header', 10, 1 );


function fituet_scientific_articles_content( $user_id ) {
	wp_enqueue_style( 'custom-publication' );
	wp_enqueue_script( 'custom-publication-js' );

	$publication_ids = fituet_get_publication_ids_by_user_id( $user_id );

	if ( count( $publication_ids ) > 0 ) {
		echo '<div class="strong">' . __( 'Scientific Articles', 'fituet' ) . '</div>';
	}

	foreach ( $publication_ids as $publication ) { ?>

		<div class="publications" data-year="<?php echo esc_attr( fituet_get_publication_year( $publication ) ); ?>">
			<div class="publication" id="pubication-<?php echo esc_attr( $publication ); ?>">
			<span class="authors">
				<?php echo fituet_get_publication_authors_html( $publication ); ?>.
			</span>
			<span class="title"><a
					href="<?php echo get_permalink( $publication ); ?>"><?php echo esc_html( get_the_title( $publication ) ); ?></a>.</span>
				<span class="year"><?php echo fituet_get_publication_year( $publication ); ?></span>
			</div>
		</div>
		<?php
	}
}

add_action( 'fituet_scientific_articles_content', 'fituet_scientific_articles_content', 10, 1 );