<?php
/**
 * Plugin Name: FIT Department
 * Plugin URI: http://thimpress.com
 * Description: FIT Department
 * Version: 1.0
 * Author: Tu TV
 * Author URI: http://tutran.me
 * License: GNU General Public License v2 or later
 */

function fituet_create_post_type_department() {

	$labels = array(
		'name'               => _x( 'Departments', 'Post Type General Name', 'fituet' ),
		'singular_name'      => _x( 'Department', 'Post Type Singular Name', 'fituet' ),
		'menu_name'          => __( 'Department', 'fituet' ),
		'name_admin_bar'     => __( 'Department', 'fituet' ),
		'parent_item_colon'  => __( 'Parent Department:', 'fituet' ),
		'all_items'          => __( 'All Departments', 'fituet' ),
		'add_new_item'       => __( 'Add New Department', 'fituet' ),
		'add_new'            => __( 'Add New', 'fituet' ),
		'new_item'           => __( 'New Department', 'fituet' ),
		'edit_item'          => __( 'Edit Department', 'fituet' ),
		'update_item'        => __( 'Update Department', 'fituet' ),
		'view_item'          => __( 'View Department', 'fituet' ),
		'search_items'       => __( 'Search Department', 'fituet' ),
		'not_found'          => __( 'Not found', 'fituet' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'fituet' ),
	);
	$args   = array(
		'label'               => __( 'Department', 'fituet' ),
		'description'         => __( 'Custom Department', 'fituet' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'menu_icon'           => 'dashicons-schedule',
	);
	register_post_type( 'fituet_department', $args );

}

add_action( 'init', 'fituet_create_post_type_department', 0 );

//require_once 'add-link-slider.php';
//require_once 'fituet-link-slider-filter.php';