/**
 * Created by tutv95 on 16/10/2015.
 */

jQuery(document).ready(function ($) {
    var owl = $(".fituet_partners_horizontal .list-partners");

    var count_partner__ = owl.attr('data-number');

    owl.owlCarousel({
        items: count_partner__, //10 items above 1000px browser width
        itemsDesktop: [1000, 5], //5 items between 1000px and 901px
        itemsDesktopSmall: [900, 3], // betweem 900px and 601px
        itemsTablet: [600, 2], //2 items between 600 and 0
        itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
        autoPlay: true
    });
});