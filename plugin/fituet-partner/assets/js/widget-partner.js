/**
 * Created by tutv95 on 16/10/2015.
 */

jQuery(document).ready(function ($) {
    var ul_list_partners = $('.fituet_partners .list-partners');
    var show_per_page_partner = ul_list_partners.attr('data-number');

    var list_partners = ul_list_partners.find('li');
    var count_partners = list_partners.length;
    if (show_per_page_partner > count_partners) {
        show_per_page_partner = count_partners;
    }

    $('.fituet_partners.vticker').vTicker('init', {
        speed: 2000,
        pause: 5000,
        showItems: show_per_page_partner,
        padding: 0
    });
});