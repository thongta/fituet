<?php
/**
 * Displays the footer sidebar of the theme.
 *
 */
?>

<?php
/**
 * fituet_before_footer_widget
 */
do_action( 'fituet_before_footer_widget' );
?>

<?php
/**
 * fituet_footer_widget hook
 *
 * HOOKED_FUNCTION_NAME PRIORITY
 *
 * fituet_display_footer_widget 10
 */
do_action( 'fituet_footer_widget' );
?>

<?php
/**
 * fituet_after_footer_widget
 */
do_action( 'fituet_after_footer_widget' );
?>