<?php
/**
 * Adds header structures.
 *
 */

/****************************************************************************************/

add_action( 'wp_head', 'fituet_add_meta', 5 );
/**
 * Add meta tags.
 */
function fituet_add_meta() {
	?>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php
}

/****************************************************************************************/

if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) : /**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 *
 * @return string The filtered title.
 */ {
	function fituet_wp_title( $title, $sep ) {
		if ( is_feed() ) {
			return $title;
		}
		global $page, $paged;
		// Add the blog name
		$title .= get_bloginfo( 'name', 'display' );
		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title .= " $sep $site_description";
		}
		// Add a page number if necessary:
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title .= " $sep " . sprintf( __( 'Page %s', 'fituet' ), max( $paged, $page ) );
		}

		return $title;
	}

	add_filter( 'wp_title', 'fituet_wp_title', 10, 2 );
	/**
	 * Title shim for sites older than WordPress 4.1.
	 *
	 * @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
	 * @todo Remove this function when WordPress 4.3 is released.
	 */
	function fituet_render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
	}

	add_action( 'wp_head', 'fituet_render_title' );
}
endif;

/****************************************************************************************/

add_action( 'fituet_links', 'fituet_add_links', 10 );
/**
 * Adding link to stylesheet file
 *
 * @uses get_stylesheet_uri()
 */
function fituet_add_links() {
	?>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php
}

/****************************************************************************************/

// Load Favicon in Header Section
add_action( 'fituet_links', 'fituet_favicon', 15 );
// Load Favicon in Admin Section
add_action( 'admin_head', 'fituet_favicon' );
/**
 * Get the favicon Image from theme options
 * display favicon
 *
 * @uses set_transient and delete_transient
 */
function fituet_favicon() {

	$fituet_favicon = '';
	if ( ( ! $fituet_favicon = get_transient( 'fituet_favicon' ) ) ) {
		global $fituet_theme_options_settings;
		$options = $fituet_theme_options_settings;

		if ( "0" == $options['disable_favicon'] ) {
			if ( ! empty( $options['favicon'] ) ) {
				$fituet_favicon .= '<link rel="shortcut icon" href="' . esc_url( $options['favicon'] ) . '" type="image/x-icon" />';
			}
		}

		set_transient( 'fituet_favicon', $fituet_favicon, 86940 );
	}
	echo $fituet_favicon;
}

/****************************************************************************************/

// Load webpageicon in Header Section
add_action( 'fituet_links', 'fituet_webpageicon', 20 );
/**
 * Get the webpageicon Image from theme options
 * display webpageicon
 *
 * @uses set_transient and delete_transient
 */
function fituet_webpageicon() {

	$fituet_webpageicon = '';
	if ( ( ! $fituet_webpageicon = get_transient( 'fituet_webpageicon' ) ) ) {
		global $fituet_theme_options_settings;
		$options = $fituet_theme_options_settings;

		if ( "0" == $options['disable_webpageicon'] ) {
			if ( ! empty( $options['webpageicon'] ) ) {
				$fituet_webpageicon .= '<link rel="apple-touch-icon-precomposed" href="' . esc_url( $options['webpageicon'] ) . '" />';
			}
		}

		set_transient( 'fituet_webpageicon', $fituet_webpageicon, 86940 );
	}
	echo $fituet_webpageicon;
}

/****************************************************************************************/

add_action( 'fituet_header', 'fituet_headerdetails', 10 );
/**
 * Shows Header Part Content
 *
 * Shows the site logo, title, description, searchbar, social icons etc.
 */
function fituet_headerdetails() {
	?>
	<?php
	global $fituet_theme_options_settings;
	$options = $fituet_theme_options_settings;

	$header_right = array(
		'logo' => $options['header_logo_right'],
		'link' => $options['header_logo_right_link'],
	);
	if ( ! isset( $header_right['link'] ) || $header_right['link'] == '' ) {
		$header_right['link'] = '#';
	}

	?>

	<div class="header__">
		<!--		<div class="top-header">-->
		<!--			<div class="container">-->
		<!--				<div class="tm-table">-->
		<!--					<div class="top-left">-->
		<!--						--><?php //if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'fituet_top_header_widget' ) ): endif; ?>
		<!--					</div>-->
		<!--					<div class="top-right">-->
		<!--						--><?php //fituet_social_links(); ?>
		<!--					</div>-->
		<!--				</div>-->
		<!--			</div>-->
		<!--		</div>-->
		<div class="container clearfix">
			<div class="hgroup-wrap clearfix">
				<hgroup id="site-logo">
					<?php
					if ( $options['header_show'] != 'disable-both' && $options['header_show'] == 'header-text' ) {
						?>
						<h1 id="site-title">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"
							   title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<?php bloginfo( 'name' ); ?>
							</a>
						</h1>
						<h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
						<?php
					} elseif ( $options['header_show'] != 'disable-both' && $options['header_show'] == 'header-logo' ) {
						?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"
						   title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
							<img src="<?php echo $options['header_logo']; ?>"
								 alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
						</a>
						<h1 id="site-title">
							<?php bloginfo( 'name' ); ?>
						</h1>
						<h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
						<?php
					}
					?>

				</hgroup>
				<!-- #site-logo -->
				<section class="hgroup-right">
					<div class="container-right">
						<?php if ( isset( $header_right['logo'] ) ) : ?>
							<a href="<?php echo esc_url( $header_right['link'] ); ?>" target="_blank">
								<img src="<?php echo $header_right['logo'] ?>" />
							</a>
						<?php endif; ?>
					</div>
				</section>
				<!-- .hgroup-right -->
			</div>
			<!-- .hgroup-wrap -->
		</div><!-- .container -->
	</div>
	<?php $header_image = get_header_image();
	if ( ! empty( $header_image ) ) :?>
		<img src="<?php echo esc_url( $header_image ); ?>" class="header-image"
			 width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>"
			 alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
	<?php endif; ?>
	<div class="affix-top">
		<?php
		if ( has_nav_menu( 'primary' ) ) {
			$args = array(
				'theme_location' => 'primary',
				'container'      => '',
				'items_wrap'     => '<ul class="root">%3$s</ul>'
			);
			echo '<nav id="main-nav" class="clearfix">
					<div class="container clearfix">';
			wp_nav_menu( $args );
			?>
			<ul class="search-btn">
				<li class="open"><a href="#"><span class="genericon genericon-search"></span></a></li>
				<li class="close"><a href="#"><span class="genericon genericon-close"></span></a></li>
			</ul>
			<div class="search_form__">
				<div class="tm-table">
					<div class="table-cell">
						<?php get_search_form(); ?>
					</div>
				</div>
			</div>
			<?php
			echo '</div><!-- .container -->
					</nav><!-- #main-nav -->';
		} else {
			echo '<nav id="main-nav" class="clearfix">
					<div class="container clearfix">';
			wp_page_menu( array( 'menu_class' => 'root' ) );
			echo '</div><!-- .container -->
					</nav><!-- #main-nav -->';
		}
		?>
	</div>

	<?php
	if ( is_home() || is_front_page() ) {
		if ( "0" == $options['disable_slider'] ) {
			if ( function_exists( 'fituet_pass_cycle_parameters' ) ) {
				fituet_pass_cycle_parameters();
			}
		}
	} else {
		if ( ( '' != fituet_header_title() ) || function_exists( 'bcn_display_list' ) ) {
			?>
			<div class="page-title-wrap">
				<div class="container clearfix">
					<?php
					if ( function_exists( 'fituet_breadcrumb' ) ) {
						fituet_breadcrumb();
					}
					?>
					<h3 class="page-title"><?php echo fituet_header_title(); ?></h3><!-- .page-title -->
				</div>
			</div>
			<?php
		}
	}
}

/****************************************************************************************/

if ( ! function_exists( 'fituet_featured_post_slider' ) ) : /**
 * display featured post slider
 *
 */ {
	function fituet_featured_post_slider() {
		global $post;

		global $fituet_theme_options_settings;
		$options = $fituet_theme_options_settings;

		$fituet_featured_post_slider = '';
		if ( ! empty( $options['featured_post_slider'] ) ) {
			$fituet_featured_post_slider .= '
		<section class="featured-slider"><div class="slider-cycle">';
			$get_featured_posts = new WP_Query(
				array(
					'posts_per_page'      => - 1,
					'post_type'           => array( 'fituet_slide' ),
					//					'post__in'            => $options['featured_post_slider'],
					'orderby'             => 'post__in',
					'suppress_filters'    => false,
					'ignore_sticky_posts' => 1                        // ignore sticky posts
				)
			);
			$i                  = 0;
			while ( $get_featured_posts->have_posts() ) : $get_featured_posts->the_post();
				$i ++;
				$title_attribute = apply_filters( 'the_title', get_the_title( $post->ID ) );
				$excerpt         = the_excerpt_max_charlength( 200 );
				if ( 1 == $i ) {
					$classes = "slides displayblock";
				} else {
					$classes = "slides displaynone";
				}
				$fituet_featured_post_slider .= '
				<div class="' . $classes . '">';
				if ( has_post_thumbnail() ) {

					$fituet_featured_post_slider .= '<figure>' . apply_filters( 'fituet_link_slider_filter_begin', '', $post->ID );

					$fituet_featured_post_slider .= get_the_post_thumbnail(
							$post->ID, 'slider', array(
								'title' => esc_attr( $title_attribute ),
								'alt'   => esc_attr( $title_attribute ),
								'class' => 'pngfix'
							)
						) . apply_filters( 'fituet_link_slider_filter_end', '', $post->ID ) . '</figure>';
				}
				if ( $title_attribute != '' || $excerpt != '' ) {
					$fituet_featured_post_slider .= '
							<article class="featured-text">';
					if ( $title_attribute != '' ) {
						$fituet_featured_post_slider .= '<div class="featured-title">' . apply_filters( 'fituet_link_slider_filter_begin', '', $post->ID ) . get_the_title() . apply_filters( 'fituet_link_slider_filter_end', '', $post->ID ) . '</div><!-- .featured-title -->';
					}
					if ( $excerpt != '' ) {
						$fituet_featured_post_slider .= '<div class="featured-content">' . $excerpt . '</div><!-- .featured-content -->';
					}
					$fituet_featured_post_slider .= '
							</article><!-- .featured-text -->';
				}
				$fituet_featured_post_slider .= '
				</div><!-- .slides -->';
			endwhile;
			wp_reset_query();
			$fituet_featured_post_slider .= '</div>
		<nav id="controllers" class="clearfix">
		</nav><!-- #controllers -->
		<div class="nav">
          <a id="prev_slide" href="#"><span class="genericon genericon-previous"></span></a>
          <a id="next_slide" href="#"><span class="genericon genericon-next"></span></a>
        </div>
		</section><!-- .featured-slider -->';
		}
		echo $fituet_featured_post_slider;
	}
}
endif;

/****************************************************************************************/

if ( ! function_exists( 'fituet_breadcrumb' ) ) : /**
 * Display breadcrumb on header.
 *
 * If the page is home or front page, slider is displayed.
 * In other pages, breadcrumb will display if breadcrumb NavXT plugin exists.
 */ {
	function fituet_breadcrumb() {
		if ( function_exists( 'bcn_display_list' ) ) {
			echo '<div class="breadcrumb">
		<ul>';
			bcn_display_list();
			echo '</ul>
		</div> <!-- .breadcrumb -->';
		}

	}
}
endif;

/****************************************************************************************/

if ( ! function_exists( 'fituet_header_title' ) ) : /**
 * Show the title in header
 */ {
	function fituet_header_title() {
		if ( is_archive() ) {
			$fituet_header_title = single_cat_title( '', false );
		} elseif ( is_search() ) {
			$fituet_header_title = __( 'Search Results', 'fituet' );
		} elseif ( is_page_template() ) {
			$fituet_header_title = get_the_title();
		} else {
			$fituet_header_title = '';
		}

		return $fituet_header_title;

	}
}
endif;
?>