<?php
/**
 * Shows the sidebar content.
 *
 */

/****************************************************************************************/

add_action( 'fituet_left_sidebar', 'fituet_display_left_sidebar', 10 );
/**
 * Show widgets of left sidebar.
 *
 * Shows all the widgets that are dragged and dropped on the left Sidebar.
 */
function fituet_display_left_sidebar() {
	// Calling the left sidebar if it exists.
	if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'fituet_left_sidebar' ) ):
	endif;
}

/****************************************************************************************/

add_action( 'fituet_right_sidebar', 'fituet_display_right_sidebar', 10 );
/**
 * Show widgets of right sidebar.
 *
 * Shows all the widgets that are dragged and dropped on the right Sidebar.
 */
function fituet_display_right_sidebar() {
	// Calling the right sidebar if it exists.
	if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'fituet_right_sidebar' ) ):
	endif;
}

/****************************************************************************************/

add_action( 'fituet_footer_widget', 'fituet_display_footer_widget', 10 );
/**
 * Show widgets on Footer of the theme.
 *
 * Shows all the widgets that are dragged and dropped on the Footer Sidebar.
 */
function fituet_display_footer_widget() {
	if ( is_active_sidebar( 'fituet_footer_widget' ) ) {
		?>
		<div class="widget-wrap">
			<div class="container">
				<div class="widget-area clearfix">
					<?php
					// Calling the footer sidebar if it exists.
					if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'fituet_footer_widget' ) ):
					endif;
					?>
				</div>
				<!-- .widget-area -->
			</div>
			<!-- .container -->
		</div><!-- .widget-wrap -->
		<?php
	}
}

?>