<?php
/**
 * Created by PhpStorm.
 * User: Tu TV
 * Date: 21/12/2015
 * Time: 2:27 AM
 */

/**
 * Adds FIT_UET_Contact_Widget widget.
 */
class FIT_UET_Contact_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'fituet_contact_widget', // Base ID
			__( 'FIT Contact', 'fituet' ), // Name
			array( 'description' => __( 'FIT Contact', 'fituet' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		?>
		<ul class="list-links">
			<?php
			if ( $instance['email'] != '' ) {
				echo '<li class="email"><span class="genericon genericon-mail"></span>' . esc_html( $instance['email'] ) . '</li>';
			}
			if ( $instance['phone_number'] != '' ) {
				echo '<li class="phone"><span class="genericon genericon-handset"></span>' . esc_html( $instance['phone_number'] ) . '</li>';
			}
			if ( $instance['address'] != '' ) {
				echo '<li class="address"><span class="genericon genericon-home"></span>' . esc_html( $instance['address'] ) . '</li>';
			}
			?>
		</ul>
		<?php
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$email = ! empty( $instance['email'] ) ? $instance['email'] : esc_html__( '', 'fituet' );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php esc_html_e( 'Email:', 'fituet' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" type="text" value="<?php echo esc_attr( $email ); ?>">
		</p>
		<?php

		$phone_number = ! empty( $instance['phone_number'] ) ? $instance['phone_number'] : esc_html__( '', 'fituet' );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'phone_number' ); ?>"><?php esc_html_e( 'Phone number:', 'fituet' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'phone_number' ); ?>" name="<?php echo $this->get_field_name( 'phone_number' ); ?>" type="text" value="<?php echo esc_attr( $phone_number ); ?>">
		</p>
		<?php

		$address = ! empty( $instance['address'] ) ? $instance['address'] : esc_html__( '', 'fituet' );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php esc_html_e( 'Address', 'fituet' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'address' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>" type="text" value="<?php echo esc_attr( $address ); ?>">
		</p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance                 = array();
		$instance['email']        = ! empty( $new_instance['email'] ) ? strip_tags( $new_instance['email'] ) : '';
		$instance['phone_number'] = ! empty( $new_instance['phone_number'] ) ? strip_tags( $new_instance['phone_number'] ) : '';
		$instance['address']      = ! empty( $new_instance['address'] ) ? strip_tags( $new_instance['address'] ) : '';

		return $instance;
	}
} // class FIT_UET_Contact_Widget

// register FIT_UET_Contact_Widget widget
function fituet_register_contact_widget() {
	register_widget( 'FIT_UET_Contact_Widget' );
}

add_action( 'widgets_init', 'fituet_register_contact_widget' );