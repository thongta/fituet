<?php
/**
 * Fituet Theme Options
 *
 * Contains all the function related to theme options.
 *
 */

/****************************************************************************************/

add_action( 'admin_enqueue_scripts', 'fituet_jquery_cookie' );
/**
 * Register jquery cookie javascript file.
 *
 * jquery cookie used for remembering admin tabs, and potential future features... so let's register it early
 *
 * @uses wp_register_script
 */
function fituet_jquery_cookie() {
	wp_register_script( 'jquery-cookie', get_template_directory_uri() . '/library/panel/js/jquery.cookie.min.js', array( 'jquery' ) );
}

/****************************************************************************************/

add_action( 'admin_print_scripts-appearance_page_theme_options', 'fituet_admin_scripts' );
/**
 * Enqueuing some scripts.
 *
 * @uses wp_enqueue_script to register javascripts.
 * @uses wp_enqueue_script to add javascripts to WordPress generated pages.
 */
function fituet_admin_scripts() {
	wp_enqueue_script(
		'fituet_admin', get_template_directory_uri() . '/library/panel/js/admin.min.js', array(
			'jquery',
			'jquery-ui-tabs',
			'jquery-cookie',
			'jquery-ui-sortable',
			'jquery-ui-draggable'
		)
	);
	wp_enqueue_script(
		'fituet_image_upload', get_template_directory_uri() . '/library/panel/js/add-image-script.min.js', array(
			'jquery',
			'media-upload',
			'thickbox'
		)
	);
}

/****************************************************************************************/

add_action( 'admin_print_styles-appearance_page_theme_options', 'fituet_admin_styles' );
/**
 * Enqueuing some styles.
 *
 * @uses wp_enqueue_style to register stylesheets.
 * @uses wp_enqueue_style to add styles.
 */
function fituet_admin_styles() {
	wp_enqueue_style( 'thickbox' );
	wp_enqueue_style( 'fituet_admin_style', get_template_directory_uri() . '/library/panel/css/admin.css' );
}

/****************************************************************************************/

add_action( 'admin_print_styles-appearance_page_theme_options', 'fituet_social_script', 100 );
/**
 * Facebook, twitter script hooked at head
 *
 * @useage for Facebook, Twitter and Print Script
 * @Use    add_action to display the Script on header
 */
function fituet_social_script() {
	?>
	<!-- Facebook -->
	<div id="fb-root"></div>
	<div id="fb-root"></div>
	<script>(function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=328285627269392";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

	<!-- Twitter -->
	<script>!function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
			if (!d.getElementById(id)) {
				js = d.createElement(s);
				js.id = id;
				js.src = p + '://platform.twitter.com/widgets.js';
				fjs.parentNode.insertBefore(js, fjs);
			}
		}(document, 'script', 'twitter-wjs');</script>

	<!-- Print Script -->
	<script src="http://cdn.printfriendly.com/printfriendly.js" type="text/javascript"></script>
	<?php
}

/****************************************************************************************/

add_action( 'admin_menu', 'fituet_options_menu' );
/**
 * Create sub-menu page.
 *
 * @uses add_theme_page to add sub-menu under the Appearance top level menu.
 */
function fituet_options_menu() {

	add_theme_page(
		__( 'Theme Options', 'fituet' ),           // Name of page
		__( 'Theme Options', 'fituet' ),           // Label in menu
		'edit_theme_options',                           // Capability required
		'theme_options',                                // Menu slug, used to uniquely identify the page
		'fituet_theme_options_do_page'             // Function that renders the options page
	);

}

/****************************************************************************************/

add_action( 'admin_init', 'fituet_register_settings' );
/**
 * Register options and validation callbacks
 *
 * @uses register_setting
 */
function fituet_register_settings() {
	register_setting( 'fituet_theme_options', 'fituet_theme_options', 'fituet_theme_options_validate' );
}

/****************************************************************************************/

/**
 * Render Fituet Theme Options page
 */
function fituet_theme_options_do_page() {
	?>

	<div id="colorawesomeness" class="wrap">

		<div class="theme-option-header">
			<div class="theme-option-block clearfix">
				<div class="theme-option-title"><h2><?php _e( 'Theme Options', 'fituet' ); ?></h2></div>
			</div>
		</div>

		<form method="post" action="options.php">
			<?php
			settings_fields( 'fituet_theme_options' );
			global $fituet_theme_options_settings;
			$options = $fituet_theme_options_settings;
			?>

			<?php if ( isset( $_GET ['settings-updated'] ) && 'true' == $_GET['settings-updated'] ): ?>
				<div class="updated fade" id="message">
					<p><strong><?php _e( 'Settings saved.', 'fituet' ); ?></strong></p>
				</div>
			<?php endif; ?>

			<div id="fituet_tabs">
				<ul id="main-navigation" class="tab-navigation">
					<li><a href="#designoptions"><?php _e( 'Main Options', 'fituet' ); ?></a></li>
					<li><a href="#featuredslider"><?php _e( 'Featured Slider', 'fituet' ); ?></a></li>
					<li><a href="#sociallinks"><?php _e( 'Social Links', 'fituet' ); ?></a></li>
					<li><a href="#other"><?php _e( 'Other', 'fituet' ); ?></a></li>
				</ul>
				<!-- .tab-navigation #main-navigation -->

				<!-- Option for Design Options -->
				<div id="designoptions">
					<div class="option-container">
						<h3 class="option-toggle"><a href="#"><?php _e( 'Header Options', 'fituet' ); ?></a></h3>

						<div class="option-content inside">
							<table class="form-table">
								<tbody>
								<tr>
									<th scope="row"><label
											for="header_logo"><?php _e( 'Header Logo', 'fituet' ); ?></label></th>
									<td>
										<input class="upload" size="65" type="text" id="header_logo"
											   name="fituet_theme_options[header_logo]"
											   value="<?php echo esc_url( $options ['header_logo'] ); ?>" />
										<input class="upload-button button" name="image-add" type="button"
											   value="<?php esc_attr_e( 'Upload Logo', 'fituet' ); ?>" />
									</td>
								</tr>
								<tr>
									<th scope="row"><label for="header_logo"><?php _e( 'Preview', 'fituet' ); ?></label>
									</th>
									<td>
										<?php
										echo '<img src="' . esc_url( $options['header_logo'] ) . '" alt="' . __( 'Header Logo', 'fituet' ) . '" />';
										?>
									</td>
								</tr>
								<tr>
									<th scope="row"><label><?php _e( 'Show', 'fituet' ); ?></label></th>
									<td>
										<input type="radio" name="fituet_theme_options[header_show]"
											   id="header-logo" <?php checked( $options['header_show'], 'header-logo' ) ?>
											   value="header-logo" />
										<?php _e( 'Header Logo Only', 'fituet' ); ?><br>

										<input type="radio" name="fituet_theme_options[header_show]"
											   id="header-text" <?php checked( $options['header_show'], 'header-text' ) ?>
											   value="header-text" />
										<?php _e( 'Header Text Only', 'fituet' ); ?><br>

										<input type="radio" name="fituet_theme_options[header_show]"
											   id="header-text" <?php checked( $options['header_show'], 'disable-both' ) ?>
											   value="disable-both" />
										<?php _e( 'Disable', 'fituet' ); ?><br>
									</td>
								</tr>

								<tr>
									<th scope="row"><label
											for="header_logo_right"><?php _e( 'Header Right', 'fituet' ); ?></label>
									</th>
									<td>
										<input class="upload" size="65" type="text" id="header_logo_right"
											   name="fituet_theme_options[header_logo_right]"
											   value="<?php echo esc_url( $options ['header_logo_right'] ); ?>" />
										<input class="upload-button button" name="image-add" type="button"
											   value="<?php esc_attr_e( 'Upload Logo', 'fituet' ); ?>" />
									</td>
								</tr>

								<tr>
									<th scope="row">
										<label for="header_logo_right_link"><?php _e( 'Link for logo right', 'fituet' ); ?></label>
									</th>
									<td><input type="text" id="header_logo_right_link" size="70"
											   name="fituet_theme_options[header_logo_right_link]"
											   value="<?php echo esc_attr( $options['header_logo_right_link'] ); ?>" />

										<p><?php _e( 'Enter your URL.', 'fituet' ); ?></p>
									</td>
								</tr>
								</tbody>
							</table>
							<p class="submit"><input type="submit" class="button-primary"
													 value="<?php esc_attr_e( 'Save All Changes', 'fituet' ); ?>" /></p>
						</div>
						<!-- .option-content -->
					</div>
					<!-- .option-container -->

					<div class="option-container">
						<h3 class="option-toggle"><a href="#"><?php _e( 'Favicon Options', 'fituet' ); ?></a></h3>

						<div class="option-content inside">
							<table class="form-table">
								<tbody>
								<tr>
									<th scope="row"><label
											for="disable_favicon"><?php _e( 'Disable Favicon', 'fituet' ); ?></label>
									</th>
									<input type='hidden' value='0' name='fituet_theme_options[disable_favicon]'>
									<td><input type="checkbox" id="disable_favicon"
											   name="fituet_theme_options[disable_favicon]"
											   value="1" <?php checked( '1', $options['disable_favicon'] ); ?> /> <?php _e( 'Check to disable', 'fituet' ); ?>
									</td>
								</tr>
								<tr>
									<th scope="row"><label
											for="fav_icon_url"><?php _e( 'Favicon URL', 'fituet' ); ?></label></th>
									<td>
										<input class="upload" size="65" type="text" id="fav_icon_url"
											   name="fituet_theme_options[favicon]"
											   value="<?php echo esc_url( $options ['favicon'] ); ?>" />
										<input class="upload-button button" name="image-add" type="button"
											   value="<?php esc_attr_e( 'Upload Favicon', 'fituet' ); ?>" />

										<p><?php _e( 'Favicon is this tiny icon you see beside URL in your browser address bar', 'fituet' ); ?></p>
									</td>
								</tr>
								<tr>
									<th scope="row"><label><?php _e( 'Preview', 'fituet' ); ?></label></th>
									<td>
										<?php
										echo '<img src="' . esc_url( $options['favicon'] ) . '" alt="' . __( 'favicon', 'fituet' ) . '" />';
										?>
									</td>
								</tr>
								</tbody>
							</table>
							<p class="submit"><input type="submit" class="button-primary"
													 value="<?php esc_attr_e( 'Save All Changes', 'fituet' ); ?>" /></p>
						</div>
						<!-- .option-content -->
					</div>
					<!-- .option-container -->

					<div class="option-container">
						<h3 class="option-toggle"><a href="#"><?php _e( 'Web Clip Icon Options', 'fituet' ); ?></a></h3>

						<div class="option-content inside">
							<table class="form-table">
								<tbody>
								<tr>
									<th scope="row"><label
											for="disable_webpageicon"><?php _e( 'Disable Web Clip Icon', 'fituet' ); ?></label>
									</th>
									<input type='hidden' value='0' name='fituet_theme_options[disable_webpageicon]'>
									<td><input type="checkbox" id="disable_webpageicon"
											   name="fituet_theme_options[disable_webpageicon]"
											   value="1" <?php checked( '1', $options['disable_webpageicon'] ); ?> /> <?php _e( 'Check to disable', 'fituet' ); ?>
									</td>
								</tr>
								<tr>
									<th scope="row"><label
											for="webpageicon_icon_url"><?php _e( 'Web Clip Icon URL', 'fituet' ); ?></label>
									</th>
									<td>
										<input class="upload" size="65" type="text" id="webpageicon_icon_url"
											   name="fituet_theme_options[webpageicon]"
											   value="<?php echo esc_url( $options ['webpageicon'] ); ?>" />
										<input class="upload-button button" name="image-add" type="button"
											   value="<?php esc_attr_e( 'Upload Web Clip Icon', 'fituet' ); ?>" />

										<p><?php _e( 'Web Clip works as shortcut to website on iOS devices home screen', 'fituet' ); ?></p>
									</td>
								</tr>
								<tr>
									<th scope="row"><label><?php _e( 'Preview', 'fituet' ); ?></label></th>
									<td>
										<?php
										echo '<img src="' . esc_url( $options['webpageicon'] ) . '" alt="' . __( 'webpage icon', 'fituet' ) . '" />';
										?>
									</td>
								</tr>
								</tbody>
							</table>
							<p class="submit"><input type="submit" class="button-primary"
													 value="<?php esc_attr_e( 'Save All Changes', 'fituet' ); ?>" /></p>
						</div>
						<!-- .option-content -->
					</div>
					<!-- .option-container -->

					<div class="option-container">
						<h3 class="option-toggle"><a href="#"><?php _e( 'Layout Options', 'fituet' ); ?></a></h3>

						<div class="option-content inside">
							<table class="form-table">
								<tbody>
								<tr>
									<th scope="row"><label><?php _e( 'Default Layout', 'fituet' ); ?></label></th>
									<td>
										<label title="no-sidebar" class="box" style="margin-right: 18px"><img
												src="<?php echo get_template_directory_uri(); ?>/library/panel/images/no-sidebar.png"
												alt="Content-Sidebar" /><br />
											<input type="radio" name="fituet_theme_options[default_layout]"
												   id="no-sidebar" <?php checked( $options['default_layout'], 'no-sidebar' ) ?>
												   value="no-sidebar" />
											<?php _e( 'No Sidebar', 'fituet' ); ?>
										</label>
										<label title="no-sidebar-full-width" class="box" style="margin-right: 18px"><img
												src="<?php echo get_template_directory_uri(); ?>/library/panel/images/no-sidebar-fullwidth.png"
												alt="Content-Sidebar" /><br />
											<input type="radio" name="fituet_theme_options[default_layout]"
												   id="no-sidebar-full-width" <?php checked( $options['default_layout'], 'no-sidebar-full-width' ) ?>
												   value="no-sidebar-full-width" />
											<?php _e( 'No Sidebar, Full Width', 'fituet' ); ?>
										</label>
										<label title="no-sidebar-one-column" class="box" style="margin-right: 18px"><img
												src="<?php echo get_template_directory_uri(); ?>/library/panel/images/one-column.png"
												alt="Content-Sidebar" /><br />
											<input type="radio" name="fituet_theme_options[default_layout]"
												   id="no-sidebar-one-column" <?php checked( $options['default_layout'], 'no-sidebar-one-column' ) ?>
												   value="no-sidebar-one-column" />
											<?php _e( 'No Sidebar, One Column', 'fituet' ); ?>
										</label>
										<label title="left-Sidebar" class="box" style="margin-right: 18px"><img
												src="<?php echo get_template_directory_uri(); ?>/library/panel/images/left-sidebar.png"
												alt="Content-Sidebar" /><br />
											<input type="radio" name="fituet_theme_options[default_layout]"
												   id="left-sidebar" <?php checked( $options['default_layout'], 'left-sidebar' ) ?>
												   value="left-sidebar" />
											<?php _e( 'Left Sidebar', 'fituet' ); ?>
										</label>
										<label title="right-sidebar" class="box" style="margin-right: 18px"><img
												src="<?php echo get_template_directory_uri(); ?>/library/panel/images/right-sidebar.png"
												alt="Content-Sidebar" /><br />
											<input type="radio" name="fituet_theme_options[default_layout]"
												   id="right-sidebar" <?php checked( $options['default_layout'], 'right-sidebar' ) ?>
												   value="right-sidebar" />
											<?php _e( 'Right Sidebar', 'fituet' ); ?>
										</label>
									</td>
								</tr>
								<?php if ( "1" == $options['reset_layout'] ) {
									$options['reset_layout'] = "0";
								} ?>
								<tr>
									<p><?php _e( 'This will set the default layout style. However, you can choose different layout for each page via editor', 'fituet' ); ?></p>
									<th scope="row"><label for="reset_layout"><?php _e( 'Reset Layout', 'fituet' ); ?>
									</th>
									<input type='hidden' value='0' name='fituet_theme_options[reset_layout]'>
									<td>
										<input type="checkbox" id="reset_layout"
											   name="fituet_theme_options[reset_layout]"
											   value="1" <?php checked( '1', $options['reset_layout'] ); ?> /> <?php _e( 'Check to reset', 'fituet' ); ?>
									</td>
								</tr>
								</tbody>
							</table>
							<p class="submit"><input type="submit" class="button-primary"
													 value="<?php esc_attr_e( 'Save All Changes', 'fituet' ); ?>" /></p>
						</div>
						<!-- .option-content -->
					</div>
					<!-- .option-container -->

					<div class="option-container">
						<h3 class="option-toggle"><a href="#"><?php _e( 'Custom Background', 'fituet' ); ?></a></h3>

						<div class="option-content inside">
							<table class="form-table">
								<tbody>
								<tr>
									<th>
										<label><?php _e( 'Change theme background', 'fituet' ); ?></label>
									</th>
									<td style="padding-bottom: 64px;">
										<?php printf( __( '<a class="button" href="%s">Click here</a>', 'fituet' ), admin_url( 'themes.php?page=custom-background' ) ); ?>
									</td>
									<td style="padding-bottom: 20px;">
									</td>
								</tr>
								</tbody>
							</table>
						</div>
						<!-- .option-content -->
					</div>
					<!-- .option-container -->
					<div class="option-container">
						<h3 class="option-toggle"><a href="#"><?php _e( 'RSS URL', 'fituet' ); ?></a></h3>

						<div class="option-content inside">
							<table class="form-table">
								<tbody>
								<tr>
									<th scope="row">
										<label for="feed-redirect"><?php _e( 'Feed Redirect URL', 'fituet' ); ?></label>
									</th>
									<td><input type="text" id="feed-redirect" size="70"
											   name="fituet_theme_options[feed_url]"
											   value="<?php echo esc_attr( $options['feed_url'] ); ?>" />

										<p><?php _e( 'Enter your preferred RSS URL. (Feedburner or other)', 'fituet' ); ?></p>
									</td>
								</tr>
								</tbody>
							</table>
							<p class="submit"><input type="submit" class="button-primary"
													 value="<?php esc_attr_e( 'Save All Changes', 'fituet' ); ?>" /></p>
						</div>
						<!-- .option-content -->
					</div>
					<!-- .option-container -->

					<div class="option-container">
						<h3 class="option-toggle"><a href="#"><?php _e( 'Homepage Post Options', 'fituet' ); ?></a></h3>

						<div class="option-content inside">
							<table class="form-table">
								<tbody>
								<tr>
									<th scope="row">
										<label
											for="frontpage_posts_cats"><?php _e( 'Homepage posts categories:', 'fituet' ); ?></label>

										<p>
											<small><?php _e( 'Only posts that belong to the categories selected here will be displayed on the front page.', 'fituet' ); ?></small>
										</p>
									</th>
									<td>
										<select name="fituet_theme_options[front_page_category][]"
												id="frontpage_posts_cats"
												multiple="multiple" class="select-multiple">
											<option value="0" <?php if ( empty( $options['front_page_category'] ) ) {
												selected( true, true );
											} ?>><?php _e( '--Disabled--', 'fituet' ); ?></option>
											<?php /* Get the list of categories */
											if ( empty( $options['front_page_category'] ) ) {
												$options['front_page_category'] = array();
											}
											$categories = get_categories();
											foreach ( $categories as $category ) :?>
												<option
													value="<?php echo $category->cat_ID; ?>" <?php if ( in_array( $category->cat_ID, $options['front_page_category'] ) ) {
													echo 'selected="selected"';
												} ?>><?php echo $category->cat_name; ?></option>
											<?php endforeach; ?>
										</select><br />
						<span
							class="description"><?php _e( 'You may select multiple categories by holding down the CTRL (Windows) or cmd (Mac).', 'fituet' ); ?></span>
									</td>
								</tr>
								</tbody>
							</table>
							<p class="submit"><input type="submit" class="button-primary"
													 value="<?php esc_attr_e( 'Save All Changes', 'fituet' ); ?>" /></p>
						</div>
						<!-- .option-content -->
					</div>
					<!-- .option-container -->
				</div>
				<!-- #designoptions -->

				<!-- Option for Featured Post Slider -->
				<div id="featuredslider">
					<div class="option-container">
						<h3 class="option-toggle"><a
								href="#"><?php _e( 'Featured Post/Page Slider Options', 'fituet' ); ?></a></h3>

						<div class="option-content inside">
							<table class="form-table">
								<tr>
									<th scope="row">
										<label><?php _e( 'Exclude Slider post from Homepage posts?', 'fituet' ); ?></label>
									</th>
									<input type='hidden' value='0' name='fituet_theme_options[exclude_slider_post]'>
									<td><input type="checkbox" id="headerlogo"
											   name="fituet_theme_options[exclude_slider_post]"
											   value="1" <?php checked( '1', $options['exclude_slider_post'] ); ?> /> <?php _e( 'Check to exclude', 'fituet' ); ?>
									</td>
								</tr>
								<tbody class="sortable">
								<tr>
									<th scope="row"><label><?php _e( 'Number of Slides', 'fituet' ); ?></label></th>
									<td><input type="text" name="fituet_theme_options[slider_quantity]"
											   value="<?php echo intval( $options['slider_quantity'] ); ?>" size="2" />
									</td>
								</tr>
								<?php for ( $i = 1; $i <= $options['slider_quantity']; $i ++ ): ?>
									<tr>
										<th scope="row"><label
												class="handle"><?php _e( 'Featured Slide #', 'fituet' ); ?><span
													class="count"><?php echo absint( $i ); ?></span></label></th>
										<td><input type="text"
												   name="fituet_theme_options[featured_post_slider][<?php echo absint( $i ); ?>]"
												   value="<?php if ( array_key_exists( 'featured_post_slider', $options ) && array_key_exists( $i, $options['featured_post_slider'] ) ) {
													   echo absint( $options['featured_post_slider'][$i] );
												   } ?>" />
											<a href="<?php bloginfo( 'url' ); ?>/wp-admin/post.php?post=<?php if ( array_key_exists( 'featured_post_slider', $options ) && array_key_exists( $i, $options['featured_post_slider'] ) ) {
												echo absint( $options['featured_post_slider'][$i] );
											} ?>&action=edit" class="button"
											   title="<?php esc_attr_e( 'Click Here To Edit' ); ?>"
											   target="_blank"><?php _e( 'Click Here To Edit', 'fituet' ); ?></a>
										</td>
									</tr>
								<?php endfor; ?>
								</tbody>
							</table>
							<p><strong><?php _e( 'How to use the featured slider?', 'fituet' ); ?></strong>

							<p />
							<ul>
								<li><?php _e( 'Create Post or Page and add featured image to it.', 'fituet' ); ?></li>
								<li><?php _e( 'Add all the Post ID that you want to use in the featured slider. Post ID can be found at All Posts table in last column', 'fituet' ); ?></li>
								<li><?php _e( 'Featured Slider will show featured images, Title and excerpt of the respected added post IDs.', 'fituet' ); ?></li>
								<li><?php _e( 'The recommended image size is', 'fituet' ); ?><strong> 1018px x
										310px.</strong></li>
							</ul>
							<p class="submit"><input type="submit" class="button-primary"
													 value="<?php esc_attr_e( 'Save All Changes', 'fituet' ); ?>" /></p>
						</div>
						<!-- .option-content -->
					</div>
					<!-- .option-container -->

					<!-- Option for More Slider Options -->
					<div class="option-container">
						<h3 class="option-toggle"><a href="#"><?php _e( 'Slider Options', 'fituet' ); ?></a></h3>

						<div class="option-content inside">
							<table class="form-table">
								<tr>
									<th scope="row"><label><?php _e( 'Disable Slider', 'fituet' ); ?></label></th>
									<input type='hidden' value='0' name='fituet_theme_options[disable_slider]'>
									<td><input type="checkbox" id="headerlogo"
											   name="fituet_theme_options[disable_slider]"
											   value="1" <?php checked( '1', $options['disable_slider'] ); ?> /> <?php _e( 'Check to disable', 'fituet' ); ?>
									</td>
								</tr>
								<tr>
									<th>
										<label
											for="fituet_cycle_style"><?php _e( 'Transition Effect:', 'fituet' ); ?></label>
									</th>
									<td>
										<select id="fituet_cycle_style" name="fituet_theme_options[transition_effect]">
											<?php
											$transition_effects = array();
											$transition_effects = array(
												'fade',
												'wipe',
												'scrollUp',
												'scrollDown',
												'scrollLeft',
												'scrollRight',
												'blindX',
												'blindY',
												'blindZ',
												'cover',
												'shuffle'
											);
											foreach ( $transition_effects as $effect ) {
												?>
												<option
													value="<?php echo $effect; ?>" <?php selected( $effect, $options['transition_effect'] ); ?>><?php printf( __( '%s', 'fituet' ), $effect ); ?></option>
												<?php
											}
											?>
										</select>
									</td>
								</tr>
								<tr>
									<th scope="row"><label><?php _e( 'Transition Delay', 'fituet' ); ?></label></th>
									<td>
										<input type="text" name="fituet_theme_options[transition_delay]"
											   value="<?php echo $options['transition_delay']; ?>" size="2" />
										<span class="description"><?php _e( 'second(s)', 'fituet' ); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row"><label><?php _e( 'Transition Length', 'fituet' ); ?></label></th>
									<td>
										<input type="text" name="fituet_theme_options[transition_duration]"
											   value="<?php echo $options['transition_duration']; ?>" size="2" />
										<span class="description"><?php _e( 'second(s)', 'fituet' ); ?></span>
									</td>
								</tr>
							</table>
							<p class="submit"><input type="submit" class="button-primary"
													 value="<?php esc_attr_e( 'Save All Changes', 'fituet' ); ?>" /></p>
						</div>
						<!-- .option-content -->
					</div>
					<!-- .option-container -->

				</div>
				<!-- #featuredslider -->

				<!-- Option for Design Settings -->
				<div id="sociallinks">
					<?php
					$social_links = array();
					$social_links = array(
						'Facebook'     => 'social_facebook',
						'Twitter'      => 'social_twitter',
						'Google-Plus'  => 'social_googleplus',
						'YouTube'      => 'social_youtube',
						'English Site' => 'social_english'
					);
					?>
					<table class="form-table">
						<div><?php _e( 'Enter URLs for your social networks e.g.', 'fituet' ); ?>
							https://www.facebook.com/ThimPress
						</div>
						<tbody>
						<?php
						foreach ( $social_links as $key => $value ) {
							?>
							<tr>
								<th scope="row"><h4><?php printf( __( '%s', 'fituet' ), $key ); ?></h4></th>
								<td><input type="text" size="45" name="fituet_theme_options[<?php echo $value; ?>]"
										   value="<?php echo esc_url( $options[$value] ); ?>" />
								</td>
							</tr>
							<?php
						}
						?>
						</tbody>
					</table>
					<p class="submit"><input type="submit" class="button-primary"
											 value="<?php esc_attr_e( 'Save All Changes', 'fituet' ); ?>" /></p>


				</div>
				<!-- #sociallinks -->

				<!-- Option for Design Settings -->
				<div id="other">
					<div class="option-container">
						<h3 class="option-toggle"><a href="#"><?php _e( 'Custom CSS (Advanced)', 'fituet' ); ?></a></h3>

						<div class="option-content inside">
							<table class="form-table">
								<tbody>
								<tr>
									<th scope="row"><label
											for="custom-css"><?php _e( 'Enter your custom CSS styles.', 'fituet' ); ?></label>

										<p>
											<small><?php _e( 'This CSS will overwrite the CSS of style.css file.', 'fituet' ); ?></small>
										</p>
									</th>
									<td>
							<textarea name="fituet_theme_options[custom_css]" id="custom-css" cols="90"
									  rows="12"><?php echo esc_attr( $options['custom_css'] ); ?></textarea>

										<p><?php _e( 'Make sure you know what you are doing.', 'fituet' ); ?></p>
									</td>
								</tr>
								</tbody>
							</table>
							<p class="submit"><input type="submit" class="button-primary"
													 value="<?php esc_attr_e( 'Save All Changes', 'fituet' ); ?>" /></p>
						</div>
						<!-- .option-content -->
					</div>
					<!-- .option-container -->
				</div>
				<!-- #other tools -->

			</div>
			<!-- #fituet_tabs -->

		</form>

	</div><!-- .wrap -->
	<?php
}

/****************************************************************************************/

/**
 * Validate all theme options values
 *
 * @uses esc_url_raw, absint, esc_textarea, sanitize_text_field, fituet_invalidate_caches
 */
function fituet_theme_options_validate( $options ) {
	global $fituet_theme_options_settings, $fituet_theme_options_defaults;
	$input_validated = $fituet_theme_options_settings;
	$input           = array();
	$input           = $options;

	if ( isset( $input['header_logo'] ) ) {
		$input_validated['header_logo'] = esc_url_raw( $input['header_logo'] );
	}

	// Header logo right
	if ( isset( $input['header_logo_right'] ) ) {
		$input_validated['header_logo_right'] = esc_url_raw( $input['header_logo_right'] );
	}
	$input_validated['header_logo_right_link'] = esc_url_raw( $input['header_logo_right_link'] );

	if ( isset( $input['header_show'] ) ) {
		$input_validated['header_show'] = $input['header_show'];
	}

	if ( isset( $options['button_text'] ) ) {
		$input_validated['button_text'] = sanitize_text_field( $input['button_text'] );
	}

	if ( isset( $options['redirect_button_link'] ) ) {
		$input_validated['redirect_button_link'] = esc_url_raw( $input['redirect_button_link'] );
	}

	if ( isset( $input['favicon'] ) ) {
		$input_validated['favicon'] = esc_url_raw( $input['favicon'] );
	}

	if ( isset( $input['disable_favicon'] ) ) {
		$input_validated['disable_favicon'] = $input['disable_favicon'];
	}

	if ( isset( $input['webpageicon'] ) ) {
		$input_validated['webpageicon'] = esc_url_raw( $input['webpageicon'] );
	}

	if ( isset( $input['disable_webpageicon'] ) ) {
		$input_validated['disable_webpageicon'] = $input['disable_webpageicon'];
	}

	//Site Layout
	if ( isset( $input['site_layout'] ) ) {
		$input_validated['site_layout'] = $input['site_layout'];
	}

	// Front page posts categories
	if ( isset( $input['front_page_category'] ) ) {
		$input_validated['front_page_category'] = $input['front_page_category'];
	}

	// Data Validation for Featured Slider
	if ( isset( $input['disable_slider'] ) ) {
		$input_validated['disable_slider'] = $input['disable_slider'];
	}

	if ( isset( $input['slider_quantity'] ) ) {
		$input_validated['slider_quantity'] = absint( $input['slider_quantity'] ) ? $input ['slider_quantity'] : 4;
	}
	if ( isset( $input['exclude_slider_post'] ) ) {
		$input_validated['exclude_slider_post'] = $input['exclude_slider_post'];

	}
	if ( isset( $input['featured_post_slider'] ) ) {
		$input_validated['featured_post_slider'] = array();
	}
	if ( isset( $input['slider_quantity'] ) ) {
		for ( $i = 1; $i <= $input ['slider_quantity']; $i ++ ) {
			if ( intval( $input['featured_post_slider'][$i] ) ) {
				$input_validated['featured_post_slider'][$i] = absint( $input['featured_post_slider'][$i] );
			}
		}
	}

	// data validation for transition effect
	if ( isset( $input['transition_effect'] ) ) {
		$input_validated['transition_effect'] = wp_filter_nohtml_kses( $input['transition_effect'] );
	}

	// data validation for transition delay
	if ( isset( $input['transition_delay'] ) && is_numeric( $input['transition_delay'] ) ) {
		$input_validated['transition_delay'] = $input['transition_delay'];
	}

	// data validation for transition length
	if ( isset( $input['transition_duration'] ) && is_numeric( $input['transition_duration'] ) ) {
		$input_validated['transition_duration'] = $input['transition_duration'];
	}

	// data validation for Social Icons
	if ( isset( $input['social_facebook'] ) ) {
		$input_validated['social_facebook'] = esc_url_raw( $input['social_facebook'] );
	}
	if ( isset( $input['social_twitter'] ) ) {
		$input_validated['social_twitter'] = esc_url_raw( $input['social_twitter'] );
	}
	if ( isset( $input['social_googleplus'] ) ) {
		$input_validated['social_googleplus'] = esc_url_raw( $input['social_googleplus'] );
	}
	if ( isset( $input['social_youtube'] ) ) {
		$input_validated['social_youtube'] = esc_url_raw( $input['social_youtube'] );
	}
	if ( isset( $input['social_english'] ) ) {
		$input_validated['social_english'] = esc_url_raw( $input['social_english'] );
	}

	//Custom CSS Style Validation
	if ( isset( $input['custom_css'] ) ) {
		$input_validated['custom_css'] = wp_kses_stripslashes( $input['custom_css'] );
	}

	// Layout settings verification
	if ( isset( $input['reset_layout'] ) ) {
		$input_validated['reset_layout'] = $input['reset_layout'];
	}
	if ( 0 == $input_validated['reset_layout'] ) {
		if ( isset( $input['default_layout'] ) ) {
			$input_validated['default_layout'] = $input['default_layout'];
		}
	} else {
		$input_validated['default_layout'] = $fituet_theme_options_defaults['default_layout'];
	}

	//RSS Service
	$input_validated['feed_url'] = esc_url_raw( $input['feed_url'] );

	//Clearing the theme option cache
	if ( function_exists( 'fituet_themeoption_invalidate_caches' ) ) {
		fituet_themeoption_invalidate_caches();
	}

	return $input_validated;
}


/**
 * Clearing the cache if any changes in Admin Theme Option
 */
function fituet_themeoption_invalidate_caches() {
	delete_transient( 'fituet_favicon' );
	delete_transient( 'fituet_webpageicon' );
	delete_transient( 'fituet_featured_post_slider' );
	delete_transient( 'fituet_socialnetworks' );
	delete_transient( 'fituet_footercode' );
	delete_transient( 'fituet_internal_css' );
	delete_transient( 'fituet_headercode' );
}


add_action( 'save_post', 'fituet_post_invalidate_caches' );
/**
 * Clearing the cache if any changes in post or page
 */
function fituet_post_invalidate_caches() {
	delete_transient( 'fituet_featured_post_slider' );
}

?>