/* Theme front end features */

/* Mobile menu
 /*! http://tinynav.viljamis.com v1.1 by @viljamis */

(function ($, window, i) {
	$.fn.tinyNav = function (options) {
		var settings = $.extend({'active': 'current-menu-item', 'header': false}, options);
		var counter = -1;
		return this.each(function () {
			i++;
			var $nav = $(this), namespace = 'tinynav', namespace_i = namespace + i, l_namespace_i = '.l_' + namespace_i, $select = $('<select/>').addClass(namespace + ' ' + namespace_i);
			if ($nav.is('ul,ol')) {
				if (settings.header) {
					$select.append($('<option/>').text('Navigation'))
				}
				var options = '';
				$nav.addClass('l_' + namespace_i).find('a').each(function () {
					options += '<option value="' + $(this).attr('href') + '">';
					var j;
					for (j = 0; j < $(this).parents('ul, ol').length - 1; j++) {
						options += '- '
					}
					options += $(this).text() + '</option>'
				});
				$select.append(options);
				if (!settings.header) {
					$select.find(':eq(' + $(l_namespace_i + ' li').index($(l_namespace_i + ' li.' + settings.active)) + ')').attr('selected', true)
				}
				$select.change(function () {
					window.location.href = $(this).val()
				});
				$(l_namespace_i).after($select);
				if (settings.label) {
					$select.before($("<label/>").attr("for", namespace_i).addClass(namespace + '_label ' + namespace_i + '_label').append(settings.label))
				}
			}
		})
	}
})(jQuery, this, 0);
jQuery(function () {
	jQuery('#main-nav .root').tinyNav({active: 'current-menu-item'})
});

jQuery(window).load(function () {
	/**
	 * Preloader
	 */
	jQuery('.wrapper').animate({opacity: 1});
	jQuery('#loader-wrapper').fadeOut();
});

jQuery(document).ready(function ($) {
	/**
	 * Animated back to top
	 */
	$(".back-to-top").hide();
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 1e3) {
				$(".back-to-top").fadeIn()
			} else {
				$(".back-to-top").fadeOut()
			}
		});
		$(".back-to-top a").click(function () {
			$("body,html,header").animate({scrollTop: 0}, 1e3);
			return false
		});
	});

	/**
	 * Tabs
	 */
	$('.tabs > span').on('click', function (e) {
		if (!$(this).hasClass('active')) {

			$('.tabs > span').removeClass('active');
			$(this).addClass('active');

			$('#profile').fadeOut(200);
			$('#scientific_articles').fadeOut(200);

			var tab = $(this).attr('data-tab');
			$('#' + tab).fadeIn(200);
		}
	});

	/**
	 * Menu affix
	 */
	var height_header__ = $('.header__').outerHeight(true);
	$(".affix-top").affix({
		offset: {
			top: height_header__
		}
	});

	$('body').on('affixed.bs.affix', function () {
		$('#branding').css('padding-bottom', 56);
	});

	$('body').on('affix-top.bs.affix', function () {
		$('#branding').css('padding-bottom', 0);
	});

	$('.search-btn .open').click(function (e) {
		e.preventDefault();

		$(".search_form__").show().animate({
			width  : 50 + '%',
			opacity: 1
		});

		$("#s__").focus();
		$("#s__").val($("#s__").val());

		$('.search-btn .open').hide();
		$('.search-btn .close').fadeIn();
	});

	$('.search-btn .close').click(function (e) {
		e.preventDefault();

		$(".search_form__").animate({
			width  : 0,
			opacity: 0
		});

		$('.search-btn .open').fadeIn();
		$('.search-btn .close').hide();
	});

	$(document).keyup(function (e) {
		if (e.keyCode == 27) {
			$(".search_form__").animate({
				width  : 0,
				opacity: 0
			});

			$('.search-btn .open').fadeIn();
			$('.search-btn .close').hide();
		}
	});

});