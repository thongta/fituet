<?php
/**
 * Display a link back to the site.
 *
 * @uses get_bloginfo() Gets the site link
 * @return string
 */
function fituet_site_link() {
	return '<a href="' . esc_url( home_url( '/' ) ) . '" title="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" ><span>' . get_bloginfo( 'name', 'display' ) . '</span></a>';
}

/**
 * Display a link to WordPress.org.
 *
 * @return string
 */
function fituet_wp_link() {
	return '<a href="' . esc_url( 'http://wordpress.org' ) . '" target="_blank" title="' . esc_attr__( 'WordPress', 'fituet' ) . '"><span>' . __( 'WordPress', 'fituet' ) . '</span></a>';
}

/**
 * Display a link to thimpress.com.
 *
 * @return string
 */
function fituet_thimpress_link() {
	return '<a href="' . esc_url( 'http://thimpress.com/wp/fituet/' ) . '" target="_blank" title="' . esc_attr__( 'ThimPress', 'fituet' ) . '" ><span>' . __( 'ThimPress', 'fituet' ) . '</span></a>';
}

?>