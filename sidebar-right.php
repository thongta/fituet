<?php
/**
 * Displays the right sidebar of the theme.
 *
 */
?>

<?php
/**
 * fituet_before_right_sidebar
 */
do_action( 'fituet_before_right_sidebar' );
?>

<?php
/**
 * fituet_right_sidebar hook
 *
 * HOOKED_FUNCTION_NAME PRIORITY
 *
 * fituet_display_right_sidebar 10
 */
do_action( 'fituet_right_sidebar' );
?>

<?php
/**
 * fituet_after_right_sidebar
 */
do_action( 'fituet_after_right_sidebar' );
?>