<?php
/**
 * This file displays page with right sidebar.
 *
 */
?>

<?php
/**
 * fituet_before_primary
 */
do_action( 'fituet_before_primary' );
?>

<div id="primary" class="no-margin-left">
	<?php
	/**
	 * fituet_before_loop_content
	 *
	 * HOOKED_FUNCTION_NAME PRIORITY
	 *
	 * fituet_loop_before 10
	 */
	do_action( 'fituet_before_loop_content' );

	/**
	 * fituet_loop_content
	 *
	 * HOOKED_FUNCTION_NAME PRIORITY
	 *
	 * fituet_theloop 10
	 */
	do_action( 'fituet_loop_content' );

	/**
	 * fituet_after_loop_content
	 *
	 * HOOKED_FUNCTION_NAME PRIORITY
	 *
	 * fituet_next_previous 5
	 * fituet_loop_after 10
	 */
	do_action( 'fituet_after_loop_content' );
	?>
</div><!-- #primary -->

<?php
/**
 * fituet_after_primary
 */
do_action( 'fituet_after_primary' );
?>

<div id="secondary">
	<?php get_sidebar( 'right' ); ?>
</div><!-- #secondary -->