<?php
/**
 * Created by PhpStorm.
 * User: Tu TV
 * Date: 29/8/2015
 * Time: 11:47 PM
 */

require_once 'fituet-teacher.php';


///**
// * Add meta box in extended profile
// */
//function fituet_bp_add_user_meta_box_scientific_articles() {
//	add_meta_box(
//		'scientific_articles',
//		__( 'Scientific articles', 'fituet' ),
//		'fituet_bp_user_inner_meta_box_scientific_articles',
//		get_current_screen()->id
//	);
//}
//
//add_action( 'bp_members_admin_user_metaboxes', 'fituet_bp_add_user_meta_box_scientific_articles' );
//
///**
// * Add visual editor for meta box "Scientific articles"
// */
//function fituet_bp_user_inner_meta_box_scientific_articles() {
//	$user_id = isset( $_GET['user_id'] ) ? $_GET['user_id'] : get_current_user_id();
//
//	wp_editor( get_user_meta( $user_id, 'fituet_scientific_articles', true ), 'fituet_scientific_articles' );
//
//}
//
///**
// * Save meta box "Scientific articles"
// */
//function fituet_bp_user_save_meta_box_scientific_articles() {
//	if ( isset( $_POST['save'] ) ) {
//		$user_id  = isset( $_GET['user_id'] ) ? $_GET['user_id'] : 0;
//		$meta_val = isset( $_POST['fituet_scientific_articles'] ) ? $_POST['fituet_scientific_articles'] : '';
//		update_user_meta( $user_id, 'fituet_scientific_articles', $meta_val );
//	}
//}
//
//add_action( 'bp_members_admin_update_user', 'fituet_bp_user_save_meta_box_scientific_articles' );

/**
 * Enqueue script custom BuddyPress
 */
function fituet_enqueue_script_buddypress() {
	if ( bp_is_user_profile() ) {
		wp_enqueue_style( 'custom-style-buddypress', get_template_directory_uri() . '/buddypress/custom-style-buddypress.css' );
	}
}

add_action( 'wp_enqueue_scripts', 'fituet_enqueue_script_buddypress' );

/**
 * Rename title Members page to Teachers
 *
 * @param $current
 * @param $id
 *
 * @return string|void
 */
function fituet_rename_the_title_members( $current, $id = null ) {
	if ( bp_is_members_directory() && $id == 0 ) {//Page buddypress has id = 0
		return fituet_get_title_teachers();
	}

	return $current;
}

add_filter( 'the_title', 'fituet_rename_the_title_members', 1, 2 );


/**
 * Rename wp_title Members page to Teachers
 *
 * @param $current
 * @param $sep
 *
 * @return string
 */
function fituet_rename_wp_title_members( $current, $sep ) {

	$blog_name = get_bloginfo( 'name' );
	if ( bp_is_members_directory() ) {
		return fituet_get_title_teachers() . ' ' . $sep . ' ' . $blog_name;
	}

	return $current;
}

add_filter( 'wp_title', 'fituet_rename_wp_title_members', 0, 2 );

/**
 * Get title "Teachers"
 *
 * @return string|void
 */
function fituet_get_title_teachers() {
	return __( 'Teachers', 'fituet' );
}