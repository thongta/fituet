<?php

/**
 * BuddyPress - Users Profile
 *
 * @package    BuddyPress
 * @subpackage bp-legacy
 */

$user_id = bp_displayed_user_id();
?>

<div id="profile">

	<!-- Name -->
	<div class="name">
		<div class="strong"><?php _e( 'Name', 'fituet' ); ?></div>
		<p><?php bp_displayed_user_fullname(); ?></p>
	</div>

	<!-- Degree -->
	<?php if ( get_user_meta( $user_id, 'fituet_degree', true ) != '' ) : ?>
		<div class="degree">
			<div class="strong"><?php _e( 'Degree', 'fituet' ); ?></div>
			<p><?php echo get_user_meta( $user_id, 'fituet_degree', true ); ?></p>
		</div>
	<?php endif; ?>

	<!-- Department -->
	<?php if ( get_user_meta( $user_id, 'fituet_department', true ) != '' ) : ?>
		<div class="department">
			<div class="strong"><?php _e( 'Department', 'fituet' ); ?></div>
			<p><?php echo esc_html( get_the_title( get_user_meta( $user_id, 'fituet_department', true ) ) ); ?></p>
		</div>
	<?php endif; ?>

	<!-- Research orientation -->
	<?php if ( get_user_meta( $user_id, 'fituet_research', true ) != '' ) : ?>
		<div class="research">
			<div class="strong"><?php _e( 'Research orientation', 'fituet' ); ?></div>
			<p><?php echo nl2br( get_user_meta( $user_id, 'fituet_research', true ) ); ?></p>
		</div>
	<?php endif; ?>

	<!-- Teaching -->
	<?php if ( ( get_user_meta( $user_id, 'fituet_teaching', true ) != '' ) ) : ?>
		<div class="teaching">
			<div class="strong"><?php _e( 'Teaching', 'fituet' ); ?></div>
			<p><?php echo nl2br( get_user_meta( $user_id, 'fituet_teaching', true ) ); ?></p>
		</div>
	<?php endif; ?>

	<!-- Scientific Articles -->
	<div id="scientific_articles">
		<?php
		do_action( 'fituet_scientific_articles_content', $user_id );
		?>
	</div>

	<!-- Email -->
	<?php if ( isset( get_userdata( $user_id )->user_email ) && get_userdata( $user_id )->user_email != '' ) : ?>
		<div class="email">
			<div class="strong"><?php _e( 'Email', 'fituet' ); ?></div>
			<p><?php echo esc_html( get_userdata( $user_id )->user_email ); ?></p>
		</div>
	<?php endif; ?>

	<!-- Website -->
	<?php if ( isset( get_userdata( $user_id )->user_url ) && get_userdata( $user_id )->user_url != '' ) : ?>
		<div class="website">
			<div class="strong"><?php _e( 'Website', 'fituet' ); ?></div>
			<p>
				<a href="<?php echo esc_url( get_userdata( $user_id )->user_url ); ?>" target="_blank"><?php echo esc_url( get_userdata( $user_id )->user_url ); ?></a>
			</p>
		</div>
	<?php endif; ?>

	<!-- Other -->
	<?php if ( ( get_user_meta( $user_id, 'fituet_teacher_other', true ) != '' ) ) : ?>
		<div class="other">
			<div class="strong"><?php _e( 'Other', 'fituet' ); ?></div>
			<p><?php echo nl2br( get_user_meta( $user_id, 'fituet_teacher_other', true ) ); ?></p>
		</div>
	<?php endif; ?>


</div><!-- .profile -->