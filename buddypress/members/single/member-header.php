<?php

/**
 * BuddyPress - Users Header
 *
 * @package    BuddyPress
 * @subpackage bp-legacy
 */

$user_id = bp_displayed_user_id();

?>

<div id="item-header-avatar">
	<?php bp_displayed_user_avatar( 'type=full' ); ?>
</div><!-- #item-header-avatar -->
<!--<div class="tabs">-->
<!--	<span class="profile active" data-tab="profile">--><?php //_e( 'View', 'fituet' ); ?><!--</span>-->
<!--	--><?php
//	do_action( 'fituet_scientific_articles_header', $user_id );
//	?>
<!--</div>-->