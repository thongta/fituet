<?php
/**
 * Displays the footer section of the theme.
 *
 */
?>
</div><!-- #main -->

<?php
/**
 * fituet_after_main hook
 */
do_action( 'fituet_after_main' );
?>

<?php
/**
 * fituet_before_footer hook
 */
do_action( 'fituet_before_footer' );
?>

<footer id="footerarea" class="clearfix">
	<?php
	/**
	 * fituet_footer hook
	 *
	 * HOOKED_FUNCTION_NAME PRIORITY
	 *
	 * fituet_footer_widget_area 10
	 * fituet_open_sitegenerator_div 20
	 * fituet_socialnetworks 25
	 * fituet_footer_info 30
	 * fituet_close_sitegenerator_div 35
	 * fituet_backtotop_html 40
	 */
	do_action( 'fituet_footer' );
	?>
</footer>

<?php
/**
 * fituet_after_footer hook
 */
do_action( 'fituet_after_footer' );
?>

</div><!-- .wrapper -->

<?php
/**
 * fituet_after hook
 */
do_action( 'fituet_after' );
?>

<?php wp_footer(); ?>

</body>
</html>