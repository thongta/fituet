<?php
/**
 * This file displays page with right sidebar.
 *
 */
?>

<?php
/**
 * fituet_before_primary
 */
do_action( 'fituet_before_primary' );
?>

<?php
/**
 * fituet_after_primary
 */
do_action( 'fituet_after_primary' );
?>

<div id="secondary">
	<?php get_sidebar( 'right' ); ?>
</div><!-- #secondary -->