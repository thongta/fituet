<?php
/**
 * Displays the header section of the theme.
 *
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<?php
	/**
	 * fituet_meta hook
	 */
	do_action( 'fituet_meta' );

	/**
	 * fituet_links hook
	 *
	 * HOOKED_FUNCTION_NAME PRIORITY
	 *
	 * fituet_add_links 10
	 * fituet_favicon 15
	 * fituet_webpageicon 20
	 *
	 */
	do_action( 'fituet_links' );

	/**
	 * This hook is important for WordPress plugins and other many things
	 */
	wp_head();
	?>

</head>

<body <?php body_class(); ?>>
<?php
/**
 * fituet_before hook
 */
do_action( 'fituet_before' );
?>

<div class="wrapper">
	<?php
	/**
	 * fituet_before_header hook
	 */
	do_action( 'fituet_before_header' );
	?>
	<header id="branding">
		<?php
		/**
		 * fituet_header hook
		 *
		 * HOOKED_FUNCTION_NAME PRIORITY
		 *
		 * fituet_headerdetails 10
		 */
		do_action( 'fituet_header' );
		?>
	</header>
	<?php
	/**
	 * fituet_after_header hook
	 */
	do_action( 'fituet_after_header' );
	?>

	<?php
	/**
	 * fituet_before_main hook
	 */
	do_action( 'fituet_before_main' );
	?>
	<div id="main" class="container clearfix">