<?php
/**
 * Template Name: Home page
 *
 * Displays the Blog with Full Content Display.
 *
 */
?>
<?php get_header(); ?>

<?php
/**
 * fituet_before_main_container hook
 */
do_action( 'fituet_before_main_container' );
?>
	<div class="homepage-template">
		<div class="top-content">
			<span class="label-highlight"><?php esc_html_e( 'Spotlight', 'fituet' ); ?></span>

			<div class="row">
				<div class="col s8 spot-light">
					<?php fituet_featured_post_slider(); ?>
				</div>
				<div class="col s4 home-info">
					<?php
					if ( have_posts() ) {
						while ( have_posts() ) {
							the_post();
							?>
							<h2 class="page-title"><?php the_title(); ?></h2>
							<div class="page-content"><?php the_content(); ?></div>
							<?php
						}
					}
					?>
				</div>
			</div>
		</div>
		<div class="bottom-content">
			<div class="row">
				<div class="col s4" id="main_button_col_1"><?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'fituet_main_button_col_1' ) ):endif; ?></div>
				<div class="col s4" id="main_button_col_2"><?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'fituet_main_button_col_2' ) ):endif; ?></div>
				<div class="col s4" id="main_button_col_3"><?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'fituet_main_button_col_3' ) ):endif; ?></div>
			</div>
			<div class="super-buttom"><?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'fituet_main_button_super' ) ):endif; ?></div>
		</div>

	</div><!-- #container -->
<?php
/**
 * fituet_after_main_container hook
 */
do_action( 'fituet_after_main_container' );
?>

<?php get_footer(); ?>